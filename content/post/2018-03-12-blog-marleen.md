---
title:  Design Challenge
date: 2018-03-12
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag heb ik met het team gewerkt aan de lifestyle journey voor de toekomstige situatie. Wij hebben hiervoor gekeken naar onze eigen huidige diarys en daar de 5 punten uit gehaald die wij graag zouden willen veranderen in de diary. De vijf punten die wij willen gaan behandelen zijn: tijd, beweging, gezondere maaltijden, uitstelgedrag/luiheid en geld.

#### Workshop data-visualisatie
In de middag had ik de workshop data-visualisatie. Hierbij heb ik geleerd dat je met het visueel uitewerken rekening moet houden met iemands 'visuele rukzak'. Dit houdt in dat als iemand bijvoorbeeld kleurenblid is je voldoende contrast in je ontwerp verwerkt, zodat hij/zij je ontwerp duidelijk kan lezen. Ik heb tijdens de workshop ook gelijk een begin gemaakt aan de visuele uitwerking van de observatie.
