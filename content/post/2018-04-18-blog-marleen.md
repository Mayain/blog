---
title:  Design Challenge
date: 2018-04-18
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Ons plan van aanpak werd door Jantien gevalideerd (feedback). We hebben veel informatie in de documentatie van het PvA maar dit staat niet duidelijk in de poster van het plan van aanpak. Dit zouden wij dus moeten verbeteren door meer informatie te verwerken op de poster. Vervolgens ben ik met het team begonnen met het opstellen van het onderzoeksplan. Hierbij hebben wij gekeken naar hoe wij onderzoek willen gaan doen in de afrikaanderwijk.
