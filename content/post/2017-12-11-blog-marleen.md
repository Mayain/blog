---
title: Sneeuwstorm
subtitle:
date: 2017-12-11
categorie: ["Design Challange OP2", "Sneeuwstorm"]
tags: ["Design Challange OP2", "Sneeuwstorm"]
---

Vandaag was ik samen met Gabrielle en Danielle aanwezig, Simon en Cesare helaas niet. Ik heb toen gezamenlijk met de dames doorgenomen wat er nog allemaal moest gebeuren voor de weken voor de kerstvakantie. Danielle Gata aan de slag met de toekomstige used journey, Gabrielle de Design rationale en ikzelf pak de style guide op.
Door de hevige sneeuwval sloot de school om 1 uur en hebben we helaas niet zo veel gezamenlijk kunnen doen.
