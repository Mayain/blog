---
title:  Design Challenge
date: 2018-05-31
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Het is vandaag de dag voor de expo. We hebben met het team verder gewerkt aan het high-fid prototype. We hebben de laaste paar schermen samen uitgewerkt tot een mooi, samenhangend geheel. Vervolgens hebben Adriana en Verena de schermen klikbaar gemaakt voor de expo. Ik  heb voor de expo nog een simpele poster in elkaar gezet ter ondersteuning van het concept.

![](Waves_poster.jpg)
_Poster_
