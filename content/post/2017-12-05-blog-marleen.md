---
title: Hoorcollege - Sociale Beinvloeding
subtitle:
date: 2017-12-05
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge OP2"]
---

Het hoorcollege vandaag ging over de sociale beïnvloeden. Aan de hand van dit college werd pas echt duidelijk hoeveel men tegenwoordig je op verschillende manieren probeerde te beïnvloeden tot bijvoorbeeld een aankoop.

Ik heb mijn testgegevens van gister omgezet in een testrapportage en hierval een analyse uitgeschreven.

![](Design Theory - sociale beinvloeding.jpg)
_Aantekeningen Hoorcollege_
