---
title: Prototype Test
subtitle:
date: 2017-09-13
categorie: ["Design Challenge", "Workshop", "Paper Prototype"]
tags: ["Prototype", "Moodboard", "Beeldanalyse", "Design Challenge", "Workshop", "Paper Prototype"]
---
### Paper prototyping
Vandaag hebben wij het paperprototype wat wij ontwikkeld hebben voorgelegd aan 2 groepen studenten. Ik heb hierbij de eerste groep van onzze studenten gefilmd. Hieruit werd al snel duidelijk dat het belangrijk is dat wij in ons prototype goede uitleg geven over hoe alles precies werkt in het spel. Wij konden merken dat namelijk veel dingen vrij onduidelijk waren voor onze testers. Daarnaast kwam de vraag van wat precies de reward is in het geheel. Dit moeten wij dus ook duidelijker naar voren laten komen.

 ![](paperprototype.jpg)
 ![](prototype_onderzoek.jpg)
 _Paper prototype_

 https://www.youtube.com/watch?v=Uka78XAqatM
 _Video test team 1_

 https://www.youtube.com/watch?v=d5QyYmwNn00
_Video test team 2_

Ook Jantien hebben wij om feedback gevraagd over ons prototype. Hierbij komt naar boven het voor haar ook vrij onduidelijk was hoe alles precies in elkaar steekt, uitleg is dus inderdaad nog iets waar wij aan moeten werken. Ook moeten wij nog er overna gaan denken

### Moodboard feedback:
Voor mijn moodboard heb ik aan Jantien ook feedbakc gevraagd. Hier was het belangrijk dat ik in mijn moodboard meer van de student terug laat zien, want monenteel was deze enkel gericht op het beroep. Hierbij kan ik ook denken aan bijvoorbeeld evenetuelle hobby's van de studenten of dingen die ze leuk vinden om te doen. Deze weizigingen zal ik nog door voeren voor het inleverpunt van de eerste iteratie.

### Workshop: Beeldanalyse
Vandaag was de tweede workshop" Beeldanalyse. Hierbij heb ik geleerd hoe je op een andere manier naar een foto kan kijken aan de hand van:
- Perspectief
- Contrast
- Compositie
- Lijnen

Wat ook een leuk feitje is om te weten is dat wij over het algemeen een foto van links boven naar recht onder lezen, net zoals wij tekst lezen.

 ![](beeldanalyse.JPG)
_Opdracht beeldanalyse_
