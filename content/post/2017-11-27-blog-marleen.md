---
title: Studiodag nr. 3
subtitle:
date: 2017-11-27
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge OP2"]
---

Vandaag heb ik met mijn team weer samengekeken naar het scrumboard en weer een nieuwe planning gemaakt voor de aankomende week. Vervolgens heb ik weer verder gewerkt aan het concept en Danielle geholpen met de invulling voor de User Journey. Hiervoor heb ik nagedacht over de touchpoints van onze doelgroep.
