---
title: Design Challenge
subtitle:
date: 2017-12-13
categorie: ["Design Challenge OP2", "Feedback", "Low-fid Prototype"]
tags: ["Design Challenge OP2", "Feedback", "Low-fid Prototype"]
---

Vandaag heb ik gezamenlijk met het team gekeken naar het concept. Momenteel willen wij in plaats van een app een uitbreiding op de website toevoegen. Hierbij speelt het porsonaliseren van de website een grote rol. Dit concept hebben wij vervolgens gezamenlijke uitgeschreven in een document. Ik ben daarna verder gegaan aan het verbeteren van de merkanalyse waar ik feedback op gekregen had.

----------
#### Feedback Wireframes en Low-fid Prototype
Vandaag heb ik mijn low-fid prototype laten valideren door Mio. Hiermee werd er ook gelijk naar de wireframes gekeken. Uit de feedback die ik ontvangen heb moet ik mijn wireframes uitgebreider uitwerken met annotaties, notities en omschrijving van hoe het gaat werken. Hierbij is de versie en wie het gemaakt heeft ook nog belangrijk.

![](Feedback analoge prototype.jpg)
_Feedback Low-fid Prototype_

![](Feedback wireframes.jpg)
_Feedback Wireframes_
