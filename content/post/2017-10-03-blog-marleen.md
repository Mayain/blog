---
title: Presentatie Voorbereiding
subtitle:
date: 2017-10-03
categorie: ["Presentatie"]
tags: ["Presentatie", "Voorbereiding"]
---

### Voorbereiding Presentatie
Voor de conceptpresentatie die wij morgen moeten doen heb ik een PowerPoint presentatie voorbereid. Ik heb geprobeerd om deze zo kort ern krachtig mogelijk in elkaar te zetten, zodat ons idee duidelijk overgebracht kan worden. Hiervoor heb ik ook ons concept geprobeerd visueel weer te geven. dit werd bij de vorige presentaties als tip meegegeven, want door dit visueel neer te zetten kan je gemakkelijker je concept overdragen naar anderen.

![](concept.png)
![](concept2.png)
_Conceptvisualizatie_
