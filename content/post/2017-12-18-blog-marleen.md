---
title: Design Challenge - Eenzame studiodag
subtitle:
date: 2017-12-18
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge Op2"]
---

Vandaag waren Danielle en Gabrielle beide met geldige reden afwezig in de studio. Simon en Cesare hadden echter niks van zich laten horen. Ik heb toe na aanleiding van het gesprek wat ik met Jantien erover gehad heb besloten om het project enkel met de meiden verder op te pakken, zodat wij hopelijk aan het eind nog een goed product neer kunnen zetten voor de expo. Als de jongens hiervoor nog iets willen doen moeten ze zelflerende het initiatief komen en er naar vragen, wij zullen dan op dat moment kijken wat de mogelijkheden zijn.

Ik ben toen begonnen met onderzoek doen naar de stijl van Paard voor de styleguide. Hierbij heb ik gekeken wat voor fonts ze allemaal gebruiken en welke kleuren er terugkomen in de stijl.
