---
title:  Design Challenge
date: 2018-02-19
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag heb ik samen met mijn team een nieuwe versie van het onderzoeksplan opgesteld. Hierbij hebben wij verschillende onderzoeksmethodes bedacht die wij uit willen gaan voeren. Wij willen onderzoek gaan doen doormiddel van een mini enquette met interviews en observatie. Wij hebben met het bedenken van de onderzoeksmethodes ook onze planning weer iets aan moeten passen.
