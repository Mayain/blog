---
title: Workshop - Letterontwerpen
subtitle:
date: 2017-12-19
categorie: ["Workshop", "Letterontwerpen"]
tags: ["Workshop", "Letterontwerpen"]
---

Vandaag was het tweede deel van de workshop letterontwerpen. Aan de hand van de informatie van de vorige keer moesten wij nu een font uitwerken doormiddel van verschillende metodes. Hierbij heb ik gebruik gemaakt van tape, kwast en inkt en spons en inkt.

![](letterontwerpen_2.jpg)
_Workshop Letterontwerpen_
