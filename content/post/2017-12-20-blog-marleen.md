---
title: Design Challenge - Kersttrui
subtitle:
date: 2017-12-20
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge Op2"]
---

Vandaag was de laatste studiodag voor de kerstvakantie. deze hebben we gezellig samen afgesloten met de stuio aan de hand van een 'bring your own lunch'. Ik had ditmaal een cake gebakken voor de lunch. Daarna was de workshop 'Hack je eigen kersttrui'. Hierbij was het de bedoeling dat je een leuke, originele kersttrui ontwierp en uitwerktte. Ikzelf heb toene en ondersteboven kerstboom ontworpen met lichtjes er in.

![](.jpg)
_Kersttrui_
