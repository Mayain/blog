---
title: Feedback Merkanalyse versie 2
subtitle:
date: 2017-12-08
categorie: ["Feedback", "Merkanalyse"]
tags: ["Feedback", "Merkanalyse"]
---

Tweede feedbackmoment voor de merkanalyse. Hierbij was mijn merkanalyse van Paard zelf goed en duidelijk, er zat een duidelijke lijn in. Echter was die van mijn doelgroep nog niet duidelijk genoeg en van de concurrent miste nog, daar moet ik dus nog aan werken.


![](Validatie feedback merkanalyse 2.jpg)
_Feedback Merkanalyse versie 2_
