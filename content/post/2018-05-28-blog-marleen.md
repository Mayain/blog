---
title:  Design Challenge
date: 2018-05-28
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Vandaag heb ik feedback gekregen op de Design Rationale van Jantien. Die mistte de ontwerpkeuzes in het document, momenteel is het meer een conceptomschrijving met onderbouwing. Ik moet met wel ontwerpen van prototypes dus onderbouwen waarom er bepaalde design keuzes gemaakt worden en deze dan weer verwerken in de design rationale met visuals er bij.

Adriana heeft de low-fid prototype schermen uitgetekend en ik ben aan de slag gegaan voor het logo van de app die wij gaan gebruiken bij het prototype. Hierbij wil ik de geluidsgolven er in terug laten komen en moet het rust uitstralen, aangezien wij het slapen (en dus rust/ontspanning) willen stimuleren. Ik heb voor het high-fid prototype een eerste opzet de 'verbinden', 'verbonden' en informatie scherm grof uitwerkt.

![](lowfid.png)
_Low-fid prototype_
