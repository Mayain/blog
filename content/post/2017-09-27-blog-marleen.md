---
title: Another Concept Brainstorm
subtitle:
date: 2017-09-27
categorie: ["Concept", "Peerfeedback"]
tags: ["Concept", "Brainstorm", "Peerfeedback"]
---

### Concept
Vandaag hebben wij ons concept weer verder uitgewerkt. We hebben gekenen hoe we het online element goed toe kunnen voegen in ons concept. Hierbij hebben wij een samensmelting gemaakt van 'The Box' en 'Steentje Bijdragen'. Van het 'Steentje bijdragen' concept hebben wij het idee van de antwoorden van quizvragen die verschijnen op verschillende telefoonschermen toegevoegd aan het 'The Box' concept. De vraag die nu nog blijft is: wat is de beloning die de leerlingen ontvangen met het openen van de doos? Hierbij kwamen wij op het idee van een maquette. Om meer samenwerking te bevordenen is en een onderdeel van een maquette die ze vervolgens met de gehele klas in elkaar moeten zetten.


----------
### Peerfeedback
Vandaag met de studiecoaching moest ik peerfeedback geven op mijn teamgenoten. Hierbij moest ik kijken naar hoe het de afgelopen periode ging en waar zij zich nog kunnen verbeteren. ikzelf kreeg ook feedback van mijn teamgenoten. hierbij is de feedback die ik ontving vrij positief. Ik bereid mij goed voor, komt afspraken en deadlines na en geef vrij vaak aan wat te doen qua planning. Mijn verbeterpunten zijn dat ik soms teveel naar problemen zoek (binnen discussies of bepaalde ideeen), waardoor mogelijk de motivatie binnen de groep omlaag gaat.

![](peerfeedback (1).jpg)
![](peerfeedback (2).jpg)

_Persoonlijke peerfeedback_
