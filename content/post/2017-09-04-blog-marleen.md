---
title: De eerste Design Challenge
subtitle:
date: 2017-09-04
categorie: ["Design Challenge", "Feedback"]
tags: ["Brainstorm", "Design Challenge", "Feedback"]
---
### Studio brainstorm
Vandaag ging onze eerste Design Challenge van start. Wij hebben  samen nagedacht voor welke doelgroep wij onze game graag zouden willen ontwikkelen en wij kwamen samen uit bij de bouwkunde studenten. Wij hebben voor deze studenten gekozen, omdat wij de architectuur van Rotterdam een interessant onderwerp vinden om te gebruiken in onze game. Aangezien architectuur goed aansluit bij bouwkunde hopen wij zo onze doelgroep door middel van architectuur weer te kunnen verbinden met de stad Rotterdam. Echter zal onderzoek uit moeten wijzen of deze doelgroep inderdaad interesse heeft voor architectuur.

![](onderzoek_doelgroep.jpg)
![](onderzoek_doelgroep_02.jpg)
_Onderzoek doelgroep_

![](onderzoek_games_hogeschool_rotterdam.jpg)
_Onderzoek games, Rotterdam en de Hogeschool Rotterdam_

Door middel van het afnemen van een interview hopen wij zo de beste/juiste informatie kunnen verzamelen over onze doelgroep. Hiervoor hebben wij gezamenlijk een vragenlijst opgesteld die wij voor gaan leggen aan een aantal bouwkunde studenten.

### Feedback
Als feedback kregen wij vandaag dat het belangrijk is om jezelf dingen af te blijven vragen: Waarom? Wat? Wie? Hoe? Hoeveel? ect. Het is ook belangrijk om tijdens het interview goed door te blijven vragen op de antwoorden die er geven worden. Dit hoop ik ook toe te passen op de interviews die wij gaan afnemen.
