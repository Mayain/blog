---
title: Design Challenge en Feedback Styleguide
subtitle:
date: 2018-01-10
categorie: ["Design Challenge OP2", "Feedback", "Styleguide"]
tags: ["Design Challenge OP2", "Feedback", "Styleguide"]
---
#### Studiecoaching
Ik heb vandaag met de studiecoaching feedback gevraagd op mijn dit-ben-ik profiel. Hierbij zijn de dingen waar ik nog aan kan werken: Het persoonlijker maken van het profiel (laat bijv. games weer terugkomen door je profiel.). Kwartaaldoelen mogen persoonlijker opgesteld worden en kunnen ook een langertermijndoel zijn.

----------
#### Design Challenge
Ik ben weer verder gegaan voor het maken van de ondersteunende visuals voor de expo van volgende week.

----------
#### Feedback styleguide
Ik heb vandaag feedback ontvangen op de styleguide. Hierbij is het grootse probleem dat ik meer een stijlanalyse van het PAARD gemaakt heb dan dat het een styleguide is geworden voor ons product. Hierbij is het belangrijk dat de stijleguide aansluiten bij wat de doelgroep terug wil zien in het product. Zo zouden wij er voor kunnen kiezen om de opzet te houden maar bijvoorbeeld het kleurgebruik aan te passen.

![](feedback styleguide.jpg)
_Feedback Styleguide_
