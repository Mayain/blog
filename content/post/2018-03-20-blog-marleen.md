---
title:  Design Challenge
date: 2018-03-20
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag heb ik het tweede deel van de workshop datavisualisatie gevolgd. Ditmaal werd gefocust op het uitwerken van de data. Hierbij moesten wij gebruik maken van inspireer afbeeldingen, en aan de hand van die afbeeldingen de data visualiseren. Door je ontwerp te baseren op afbeeldingen ga je sneller out of the box denken met je ontwerp. Hierbij moet je jezelf dus vrijlaten om een af te stappen van de traditionele manier van het uitwerken met bijvoorbeeld een staafdiagram.

![](datavisualisatie2.jpg)
_Data-visualisatie_
