---
title:  Design Challenge
date: 2018-03-07
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag heb ik gewerkt aan mijn lifestyle diary. Ik heb de afgelopen 48uur bijgehouden wat ik zoal gegeten, bewogen en ontspannen heb. Van mijn eten heb ik ook foto’s gemaakt en deze uitgeprint, en als een ‘tijdslijn collage’ uitgewerkt wat ik die afgelopen 48 uur heb gegeten, bewogen en ontspannen. Hierbij speelden media prikkel ook nog een rol. Deze heb ik ook verwerkt in mijn diary.

![](lifestylediary1.jpg)
_Lifestyle diary_
