---
title:  Design Challenge
date: 2018-04-23
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Ik heb voor de hervalidatie van het plan van aanpak een nieuwe versie uitgewerkt. Hierbij heb ik met het uitwerken gelet op de indeling en opmaak van de poster. Ik heb hierbij aan de hand van de vorige feedback meer informatie verwekt in de poster. Vervolgens moesten wij dit gezamenlijk presenteren aan Jantien en Robin. Als feedback zeiden zij dat wij nog moeten kijken naar de onderzoeksvragen en deze meer te richten op slaap.

's Middags zijn wij met z’n allen naar Rotterdam zuid toegegaan om de afrikaanderwijk te bekijken om zo onze doelgroep beter te begrijpen en te kijken waar wij hun zouden kunnen vinden.

![](Plan van aanpak V3.1.jpg)
_Plan van aanpak_
