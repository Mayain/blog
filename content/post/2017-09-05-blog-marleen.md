---
title: Het Ontwerpproces
subtitle:
date: 2017-09-05
categorie: ["Hoorcollege", "Onderzoek", "Interview", "Huiswerk"]
tags: ["Design", "Ontwerpproces", "Doelgroeponderzoek", "Hoorcollege", "Onderzoek", "Interview"]
---
### Hoorcollege: _Het Ontwerpproces_
Vandaag hadden wij ons eerste echte hoorcollege. Het onderwerp wat vandaag aan werd gesneden was: het ontwerpproces. Om het ontwerpproces goed te kunnen begrijpen is het eigenlijk ook van belang om te begrijpen wat DESIGN precies inhoud. Een mooie quote die dat alles eigenlijk samenvat is:

_"Design is to design a design to produce a design"_

![](dummy_aantekeningen.jpg)
![](dummy_aantekeningen_02.jpg)
_Aantekeningen hoorcollege_

### Doelgroeponderzoek
s'Middags hebben zijn wij met onze groet naar de locatie van de bouwkunde studenten geweest. Hier hebben wij een aantal bouwkunde studenten mogen interviewen. Het is bij het interviewen belangrijk dat je goed door probeert te vragen op de antwoorden die er gegeven worden. Dit is echter niet altijd even makkelijk, want het hangt van de antwoorden af of je hier echt op door kan vragen of niet. Ook merk je snel dat je wel naar het gemak van je vragenlijst terug gaat.

![](interview_bouwkunde_1.jpeg)
_Interview bouwkundestudent_

### Huiswerk
Thuis heb ik mij blogboek bijgewerkt en eerste opzet gemaakt voor mijn moodboard. Voor mijn moodboard heb ik inspiratie gehaald over de verschillende takken van bouwkunde en wat precies je functie is binnen deze tak. Het gebruik van materiaal is ook een belangrijk punt bij bouwkunde. Dit wil ik dus ook graag terug laten komen bij het ontwerp van mijn moodboard.
