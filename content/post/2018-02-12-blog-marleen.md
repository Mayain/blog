---
title:  Verdeling nieuwe teams
subtitle:
date: 2018-02-12
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag kregen wij de teamverdeling voor het aankomende project. Ik ben ingedeeld in het team: Bram en de sterke vrouwen. Dit team bestaat uit: Bram, Verena, Adriana en Imra. Echter besloot Imra gelijk om naar een ander team over te stappen, dus zijn wij nu een team van 4. We zijn gelijk gezamenlijk begonnen met het opstellen van een plan van aanpak voor het eerste kwartaal van het project. Voor het indelen van het ontwerpproces heb ik gekeken naar de beroepsproducten die wij zouden kunnen gebruiken voor de indeling hiervan. Op die manier kunnen wij ook weer de producten gebruiken voor het aantonen van onze competenties.
