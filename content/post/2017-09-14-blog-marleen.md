---
title: FAILED Tools for Design
subtitle:
date: 2017-09-14
categorie: ["Design Challenge", "Visueel Analyse"]
tags: ["Failed", "Onderzoek", "Design Challenge", "Analyse"]
---

### FAILED Tools for Design
Vandaag zou ik mijn eerste les voor InDesign krijgen doormiddel van het Tool for Design programma op school. Echter had de persoon die ons les zou geven zich verslapen, dus dit ging toen niet door. deze tijd heb ik vervolgens gebruikt om verder te werken aan het visueel maken van mijn persoonlijk onderzoek. Ik heb hiervoor onderzoek gedaan naar de soort gebouwen die er allemaal in Rotterdam zijn. Hierbij kwam ik uit op 5 kernwoorden:

- **Rotterdam**: Het onderwerp is Rotterdam
- **Oud & Nieuw**: De gebouwen die in Rotterdam staan verschild enorm. Er zijn veel oude maar ook veel nieuwe gebouwen.
- **Modern & Klassiek**: Door de gevarieerde bouwstijlen die door Rotterdam verspreidt staan heb je zowel veel modern als klassiek ogende gebouwen.
- **Bombardement**: het bombardement wat plaat heeft gevonden in Rotterdam tijdens de tweede wereldoorlog is een van dingen die veel invloed heeft gehad op hoe de stad er nu uit ziet.
- **Skyline**: Door de grote variatie en ook hoogbouw in Rotterdam in Rotterdam eigenlijk de enige stad in Nederland die een echte skyline heeft.

----------
### Design Theorie
Vandaag moesten we aan de hand van de theorie die wij afgelopen dinsdag gehad hebben (Gestalt, Semiotiek en Retorica wetten) foto's goed bekijken en ontleden. Hierbij moesten we goed kijken en nadenken of er wetten toegepast konden worden op de foto en waarom deze wet er bij past. Dit is nog is in sommige gevallen nog niet zo simpel als het lijkt, want de kan sommige van de wetten makkelijk met elkaar verwarren/ door elkaar halen. Door hier bij vaker stil te staan en eens te denken wat mogelijk ergens bij past zal het makkelijker worden om het te begrijpen en eventueel later toe te passen. Ook moesten we aan de hand van een woord binnen 10 seconden tekenen wat er in je op kwam. Dit deden we een keer met fineliner en dezelfde woorden nogmaals met een marker. Hierbij is duidelijk te zien dat je met een marker minder detail aan zou brengen dan als je met een fineliner tekent. Binnen de groep wat te zien dat wij algemene teken , zoals bijvoorbeeld pijlen voor snelheid, al snel koppelen aan bepaalde woorden.

![](beeldontleding_02.JPG)
![](beeldontleding_01.JPG)
_Ontleding foto's_

![](woordtekeningen.JPG)
_Tekeningen van woorden_

----------
### Huiswerk
Thuis heb ik nog verder gewerkt aan mijn dit ben ik-profiel. Het reflecteren op de onderzoeksantwoorden ging bij mij vrij makkelijk. Echter vindt ik het bedenken van de kwartaaldoelen en stuk moeilijker. Dit is omdat je hier echt een stuk dieper op in moet gaan waarom, hoe en wanneer je iets wil gaan doen en op welke manier je dit dan zou kunnen meten. Omdat ik zelf meer een doener ben dan een denker is dit best wel een puntje waar ik meer op zou kunnen oefenen.
