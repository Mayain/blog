---
title:  Design Challenge
date: 2018-05-22
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Ik heb vandaag met Adriana en Bram verder nagedacht over het concept. Afgelopen woensdag hebben zij de concepten voorgelegd bij Jantien en de doelgroep. De doelgroep had voorkeur voor het concept met de brainwaves, zij vonden dit zeer interessant. Wij zijn hier met z’n drieën verder over na gaan denken hoe dit concept precies vorm zou kunnen krijgen. Aan de hand van het concept ben ik deze verder uit gaan schrijven in de design rationale. Hierbij heb ik ook gekeken naar de onderzoeksresultaten en hoe het aansluit bij de doelgroep/gebruiker.
