---
title:  Kick-off OP3
subtitle:
date: 2018-02-06
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag kregen wij de briefing voor onze nieuwe opdracht voor OP3. Het hoofdonderwerp hiervoor is: gezonde levensstijl. De case die wij voorgelegd krijgen is: ‘Hoe kunnen wij jongeren van Rotterdam (Zuid) motiveren een gezonde levensstijl aan te nemen?’. Hierbij zal ik eerst onderzoek bij mijn eigen levensstijl moeten doen en die moeten vergelijken met dat van mijn nieuwe teamgenoten. Momenteel weet ik nog niet wie mijn teamgenoten worden, dat zal pas volgende week duidelijk zijn.
