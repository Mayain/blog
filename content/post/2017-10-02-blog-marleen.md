---
title: Paper Prototype Test
subtitle:
date: 2017-10-02
categorie: ["Design Challenge", "Paper Prototype", "Workshop","Blog"]
tags: ["Onderzoeken", "Paper Prototype", "Design Challenge", "Workshop", "Blog"]
---

### Paper Prototype Test
Vandaag hebben wij met onze groep een test gedaan van ons paper prototype.

In tegenstelling tot het vorige paper prototye hebben wij ditmaal enkel de schermen getoond die op dat moment van toepassing waren. Dit zorgt al voor minder verwarring dan wat er bij de vorige keer met het testen van het paper prototype gebeurde.

----------
### Workshop: Blog Thema
s'Middags had ik een workshop voor het instellen van thema's voor het blog.
