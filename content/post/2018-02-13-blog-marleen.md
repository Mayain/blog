---
title:  Hoorcollege artifacten
date: 2018-02-13
categorie: ["Design Theory OP3"]
tags: ["Design Theory OP3"]
---

Vandaag hadden wij een hoorcollege over artifacten. Dit zijn door de mens gemaakte producten. De ontwikkeling en design van artifacten wordt beïnvloed door de beschikbare materialen. Bij dit hoorcollgege kon je in de presentatie duidelijk zien hoe in de loop van de jaren het materiaal design heeft beinvloed.
