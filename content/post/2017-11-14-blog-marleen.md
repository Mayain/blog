---
title: Kick-off project 2
subtitle:
date: 2017-11-14
categorie: ["Kick-off", "Design Challenge OP2", "Keuzevak"]
tags: ["Kick-off", "Design Challenge OP2", "Keuzevak"]
---

Vandaag had ik de kick-off van het nieuwe project. Hiervoor moest in naar PAARD in Den Haag. Hier kregen wij een presentatie van de studio Fabrique en PAARD. Zij zullen onze opdrachtegevers zijn voor het aankomende project.

Fabrique is een design/ontwerpkantoor die zich in een aantal verschillende steden bevind. fabrique gelooft in het ceeren van impact door middel van design. Hun motto is daarbij: "Challenge reality". PAARD is een poppoduim in Den Haag. Naast het poppoduim hebben ze ook nog een cafe waar locale (onbekende) bands optredenen en een aantal verschillende festivals die zij organiseren. Zij hadelen vanuit 4 punten: Geluk, trots, intiem en verrassing.

De kwestie die waar wij nu iets voor gaan ontwikkelen is: Hoe kan PAARD bezoekers stimuleren om vaker terug te komen?

----------
#### Japans les 1
Na de presentatie in PAARD had ik mijn eerste keuzenvak: Japans. In deze les heb ik behandeld welk geschrift wij gaan leren lezen en schrijven (Hiragana), hoe de zinsopbouw werkt, hoe je telt en vervolgens dus ook klok kan kijken. Ik vindt deze lessen al gelijk erg pittig qua stof en zal hier dus goed de moeite voor moeten doen om dit te leren.
