---
title: Hoorcollege - Visuele Beinvloeding
subtitle:
date: 2018-01-09
categorie: ["Hoorcollege", "Design Theory OP2"]
tags: ["Hoorcollege", "Design Thoery OP2"]
---

Het gemiste hoor college van voor de kerstvakantie werd vandaag behandeld. Het ging ditmaal over visuele beïnvloeding: hoe wordt men beïnvloed aan de hand van beeld. Het belangrijkste hiervan is het feit dat in het deel Vd hersens waar beeld wordt verwerkt de rechterhersenhelfd is, hier zitten ook en emoties en dus wordt het dan snel en ondoordacht verwerkt.

![](Design Theory - visuele beinvloeding.jpg)
_Aantekeningen Hoorcollege_
