---
title: Onderzoeksproces
subtitle:
date: 2017-09-07
categorie: ["Design Theory"]
tags: ["Onderzoeksproces", "Visualiseren", "Design Theory"]
---
### Design Theory
Vandaag was onze eerste dag design theorie. Het onderwerp wat we besproken hebben is: het onderzoeksproces. Aan de had van de theorie die wij afgelopen dinsdag met het hoorcollege gehad hebben moesten wij een zo'n design proces visualiseren aan de hand van een situatie die je zelf eens een keer meegemaakt hebt. Hierbij heb ik gekozen voor het visualiseren van het proces wat komt kijken bij het maken van een cosplay kostuum. Het war hierbij belangrijk om goed te kijken naar de theorie en termen die bij het hoorcollege gegeven zijn. Vervolgens moesten we als groep zijnde een van de vijf concepten uitkiezen en deze vervolgens verder uitwerken. Wij hebben toen gekozen om mijn proces verder uit te werken, omdat het proces uit veel (en goede toepasselijke) stappen bestond. Nadat we het proces gepresenteerd hadden kregen wij de feedback dat het goed gevisualiseerd was en duidelijk in kaart gebracht.

Na de design theorie hebben wij met onze groep nog een aantal groepsregels opgesteld voor onze groepsposter.

![](onderzoeksproces.jpg)
_Onderzoeksproces_
