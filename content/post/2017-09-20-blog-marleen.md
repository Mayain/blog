---
title: CMD Beroepsprofiel
subtitle:
date: 2017-09-20
categorie: ["Design Challenge","Studie Coaching"]
tags: ["Interview nr. 2","CDM Beroepsprofiel", "Design Challenge","Studie Coaching"]
---

### Design Challenge: Another interview
Vandaag zijn wij weer bij de locatie van bouwkunde langs geweest om weer een nieuwe interview af te nemen bij de studenten. Ditmaal lag de focus van het interview meer op de interesses van de studenten. Ik had dit keer twee jogens van je tweede jaar bij mij aan tafel zitten: Marvin en Micha. Twee jongens die biede van gamen houden, maar het toch wel jammer vinden dat ze eigenlijk minder buiten komen dan ze vroeger deden. Hopelijk kunnen we na het samenvoegen en anayseren van de interviews weer tot bruikbare conclusies komen waarmee we een mooi concept kunnen vormen. Op de school waar de Bouwkunde stundenten les hebben heb ik ook een aantal foto's gemaakt van schetsen en materialen die gebruikt worden bij bouwkunde.

----------
### CDM Beroepsprofiel
Met de studie coaching hebben wij het dit keer gehad over het CDM beroepsprofiel. Hierbij moesten we refecteren over hoe een typische CDMer er uit ziet en wat kernmerkende eigenschappen zijn van een CMDer.

![](cmd beroepsprofiel (1).JPG)
![](cmd beroepsprofiel (2).JPG)
_CMD Beroepsprofiel_
