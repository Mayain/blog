---
title: 0-Meting Tekenen
subtitle:
date: 2017-09-29
categorie: ["0-Meting Tekenen"]
tags: ["0-Meting", "Paper Prototype"]
---

### 0-Meting Tekenen
Vandaag hadden wij onze 0-meting tekenen. Hierbij hed ik gekozen om mijn hand na te tekenen die iets vasthoud en het verplichte storyboard. Het was hierbij moeilijk voor mij om doormiddel van arceren schaduwen aan te brengen, omdat ik normaal gewend ben met tekenen om de vegen voor het creaeren van schaduwen wat nu niet toegestaan was.

----------
### Paper Prototype
Na de 0-meting heb ik thuis gewerkt aan het paper prototype. Hiervoor heb ik op karton een uitvouw van een kleine doos gemaakt. Deze heb ik vervolgens uitgesneden en in elkaar gezet. Ook heb ik uit karton een uitsnede van een telefoon gemaakt, zodat het voor de testgroep duideijk is dat het gaat draaien om een app.

![](paper_prototype (1).JPG)
![](paper_prototype (2).JPG)
_Paper Prototype_
