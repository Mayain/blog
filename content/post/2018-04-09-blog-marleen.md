---
title:  Design Challenge
date: 2018-04-09
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Vandaag was de Kick-off voor de nieuwe periode. Wij gaan ons nu focussen op de jongeren in Rotterdam Zuid. Hiervoor heeft Joke een presentatie gegeven ter inleiding van de nieuwe periode. In de studio werden de diverse wijken verdeeld over de teams d.m.v loting. Ik heb met mijn team de Afrikaanderwijk toegewezen gekregen. Ik heb over deze wijk een video doorgekeken.
