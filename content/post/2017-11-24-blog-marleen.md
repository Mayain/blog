---
title: Validatie Merkanalyse
subtitle:
date: 2017-11-24
categorie: ["Engels", "Validatie", "Merkanalyse", "Feedback"]
tags: ["Engels", "Validatie", "Merkanalyse", "Merkanalyse"]
---
#### Engels
Vandaag hadden wij onze eerste les Engels. Wij hebben de theorie behandeld en toegepast over de veschillende tijdvormen van een zin:
- Present Simple: Habit or fact - ww+ s/es
- Present Continuous: Right now or planning to do something (future) - ww+ ing
- Past Simple: Something happened in the past and stays in the past - ww+ d/ed (will, are ect.)
- Past Continuous: Happend in the past, but non-stop - ww+ ing
- Present Perfect: Something started in the past and is still going on or something happend in the past and is still visible - has/have
- Past Perfect: Something happend in the past before the other - had

----------
#### Validatie merkanalyse
Vandaag had ik de validatie van het merkanalyse. Deze is nog niet voldoende en hier moet ik dus nog aan werken voor ik het in mijn leerdossier kan gebruiken. Als feedback heb ik ontvangen dat mijn doelgroep iets te breed is, dat het kernconcepts klein en pakkend moet zijn, het merkmodel moet uniek zijn en anders dan de concurrenten (concurrenten onderzoeken) en ik specifieker moet zijn met de termen die ik gebruik.

Aan het voorbeeldmodel kon je echt zien dat het van breed naar smal toeloopt, dus van veel naar weinig (pakkende) informatie.

![](Validatie feedback merkanalyse 1.jpg)
_Feedback Merkanalyse versie 1_
