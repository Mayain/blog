---
title: 0-Meting Nederlands - Engels
subtitle:
date: 2017-10-05
categorie: ["0-Meting", "Paper Prototype", "Design Theory", "Tools for Design"]
tags: ["0-Meting", "Paper Prototype", "Design Theory", "Tools for Design"]
---
### Tools for Design - Indesign
Vandaag ging de InDesign les over de texttool. Met deze les werd het voor mij al meer duidelijk waarom InDesign veen gebruikt wordt voor drukwerk. Met de texttool kan je heet veel dingen met rondom tekst aanpassen. Dit is een stuk uitgebreider dan in de andere adobe programma's. Ditmaal waren de opdrachten het maken van een flyer en het namaken van een poster. Dit ging mij ook weer goed af.

----------
### 0-Meting Nederlands-Engels
Vandaag hadden wij de 0-meting tekenen. Deze verliep wel oke. Ik kon wel merken dat doordat je tegenwoordig vrijwel alles digitaal doet je hierdoor heel erg lui wordt, want alles wat je tegenwoordig gebruikt heeft een vorm van spellingscontrole. Hierdoor ben je zelf niet meer bezig met spelling.

----------
### Paper Protoype Theory
Na de 0-meting Nederlands-Engels hadden wij nog de Design Theory van paper prototyping. Hierbij was de opdracht om met een object (koffiezetapparaat, vuilnisbakken, plant, stoel of deur) leerlingen met elkaar te verbinden. Heirbij moesten wij de double diamond theorie toepassen. Eerst moesten wij zoveelmogelijk ideeen opschrijven, catagoriseren in verschillende catagorien (makkelijk uit te voeren makkelijk idee, origineel idee, toekomstig idee die nu nog  niet uitvoerbaar is). Vervolgens kregen wij 20 min. om 2 ideeen uit te werken tot een paper prototype maken. Hierbij was een idee van een handvat in de vorm van een hand. Wij hoopten dat dit een gespreksonderwerp zou worden onder onze testgroep. Het tweede idee was een deur waar men op kan tekenen, doordat de leerlingen er gezamelijk op tekenen komt er een bepaalde interactie tot stand.

Met het snelle testen van de 2 paper prototypes werd al snel duidelijk dat het handvat in deze tijd en vorm niet aanzet tot een gespreksonderwerp. Echter bij het tekenen op de deur kom je merken dat onze verwachtingen wel redelijk tot stand kwamen. De testgroep praten met elkaar terwijl ze aan het tekenen waren en af en toe werd er ook gelachen. Als we dan eventueel kijken naar de volgende stappen die wij zouden kunnen ondernemen met een concept als deze is dat de gehele deur het canvas wordt i.p.v. het A3 formulier wat wij nu gebruikt hadden en dat men kan tekenen met whiteboard makers. Op die manier kunnen de tekeningen continu vernieuwd worden en kunnen eventuele kwetsende tekeningen of woorden ook verwijdert worden.

Bij deze oefening kon je merken dat je in een anderhalf uur al een snel prototype in elkaar kan zetten waarmee je vervolgens snel aan de slag kan gaan.

![](theory_paper_prototype (1).JPG)
![](theory_paper_prototype (2).JPG)
![](theory_paper_prototype (3).JPG)
![](theory_paper_prototype (4).JPG)
![](theory_paper_prototype (5).JPG)
_Paper Prototypes_
