---
title:  Design Challenge
date: 2018-03-14
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag moest ik samen met mijn team de toekomstige lifestyle diary en de ontwerpt rationale opstellen. Aan de hand van de verzamelde onderzoeksdata heb ik samen met Verena dit uitgeschreven. Vervolgens hebben wij samen met Philip en Bram nagedacht over de persona’s die wij kunnen gebruiken voor het concept. Een jongen en een meisje, omdat er veel verschillen zitten tussen de twee. Wij hebben aan de hand van een lijst nagedacht over relevante informatie die wij kunnen gebruiken en ons ingeleefd in de personen.
