---
title: Nieuwe teams
subtitle:
date: 2017-11-15
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge OP2"]
---

Vandaag leerde ik mijn nieuwe team kennen voor het aankomende project. Voor het aakomende project werk ik samen met: Ceasare, Simon, Danielle en Gabrielle. Wij hebben eerst binnen het team geinventariseerd wat zoal onze sterke en zwakken punten zijn. Hierdoor kwamen wij er achter dat wij elkaar mogelijk goed aan kunnen vullen in het team. Zo ben ik zelf iemand die georganiseerd te werk gaat waar Ceasare en Danielle weer meer chaoten zijn. Hier ligt dus de mogelijkheid dat wij elakaar eventueel daarin zouden kunnen helpen en ondersteunen. Daarnaast hebben wij gewoon een gezellig praatje met elkaar gemaakt.
