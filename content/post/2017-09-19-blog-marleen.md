---
title: Opnieuw Onderzoeken
subtitle:
date: 2017-09-19
categorie: ["Hoorcollege", "Onderzoek"]
tags: ["Prototyping", "Restart", "Hoorcollege", "Onderzoek"]
---

### Hoorcollege: Prototyping
Vandaag hadden wij een hoorcollege over prototyping. De essentie hiervan is om tot een goed prototype te komen moet je breed beginen en langzaam toewerken naar een punt. Als je gelijk van de oplossing naar het probleem toe gaat kan het zijn dat je wellicht betere oplossing niet tegenkomt. Als je dan dus eerst je bilk verwijdt en vanuit verschillende prespectieven naar dingen gaat kijken kan dit wellicht leiden tot een beter eindresultaat.

![](prototyping.jpg)
_Aantekeningen Prototyping_

----------
### Nieuw onderzoek starten
Na het treurige KYD nieuws van gister zijn wij samen gaat zitten met de groep, denkende over wat wij weer opnieuw willen gaan onderzoeken over onze doelgroep. Wij willen hiervoor een nieuwe interviews af gaan nemen om zo meer over de interesses en hobby's van de studenten te weten te komen. Dit is iets wat wij bij onze vorig interview eigenlijk wel miste. Ik hoop dit keer bij het interview ook beter door te kunen vragen dan de vorige keer en zo ook meer te weten te komen over de student.
