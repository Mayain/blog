---
title: Leerdossier
subtitle:
date: 2017-10-11
categorie: ["Studie Coaching"]
tags: ["Leerdossier", "Studie Coaching"]
---

### Leerdossier
Vandaag hebben wij meer informatie ontvangen betreffende het leerdossier. Voor het eerste kwataal zullen wij vooral beoordeeld worden voor onze inzert tijdens de afgelopen periode. Hierbij is het dus vooral belangrijk dat ik leer te begrijpen hoe je het leerdossier precies opbouwd en wat hier precies in verwerkt moet worden. Hierbij is het reflecteren op jezelf een belangrijk onderdeel. Ook is het van toepassing dat je de evnetuele dingen die je toe wilt voegen laat valideren voor je leerdossier.
