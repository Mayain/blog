---
title:  Design Challenge
date: 2018-03-21
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag de tweede keer feedback op mijn lifestyle diary ontvangen. Ditmaal is hij voldoende beoordeeld, echter zou er nog meer focus moeten liggen op de prikkel en de gedachten hierachter, omdat je als ontwerper juist die prikkels wilt kunnen begrijpen en beïnvloeden.

Feedback gezamenlijke lifestyle diary: Joke was erg enthousiast. Vooral het uitstelgedrag/luiheid, wat als een soort van sneeuwbaleffect ontstaat door het te weinig slaap is een interessant onderwerp waar wij verder op in willen gaan.

Ik heb met het team nog gewerkt aan een vernieuwde versie van de ontwerpcriteria. Deze hebben wij weer aangepast aan de hand van de ontvangen feedback van de lifestyle diary.
