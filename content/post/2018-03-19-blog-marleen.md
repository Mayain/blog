---
title:  Design Challenge
date: 2018-03-19
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag heb ik met het team (behalve Bram, die was helaas ziek) een brainstorm sessie gedaan voor het bedenken van het concept. We hebben een techniek gebruikt die ik eerder eens geleerd heb bij een werkcollege OP1 (prototyping). Aan de hand van de ideeen die wij bedacht hebben, hebben wij er 6 uitgekozen die wij iets verder uit willen werken.

Validatie van de ontwerpcriteria: feedback gekregen dat dat wat wij nu opgezet hadden was meer een onderzoeksanalyse dan ontwepcriteria. Bij ontwepcriteia moet het specifieker, punten die je af kunt vinken en momenteel enkel op conceptueel niveau.
