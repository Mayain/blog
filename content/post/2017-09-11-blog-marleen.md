---
title: Concept Brainstorm
subtitle:
date: 2017-09-11
categorie: ["Concept ontwikkelen", "Design Challenge", "Workshop"]
tags: ["Concept", "Blog", "Design Challenge", "Workshop"]
---
### Concept ontwikkelen
Afgelopen vrijdag hebben wij ieder individueel een concept bedacht aan de hand van het interviewanalyse. Vandaag hebben wij de concepten aan elkaar voorgelegd en uiteindelijk een concept uitgekozen om verder meer te werken. Dit concept van Evelien draait om een kaartspel waarbij grondstoffen verzameld moeten worden waarmee dan vervolgens gebouwen gebouwd kunnen worden. Dit concept mistte echter nog een online aspect. wij hebben geprobeerd om dit concept verder uit te werken. Hierbij merken wij wel dat het moeilijk is om een goed concept in een vrij korte periode uit te werken.

Als feedback kregen wij vandaag dat het belangrijk is dat de studenten die het spel gaan spelen de stad ook ervaren, en niet alleen zien aan de hand van de speelkaarten. We moeten ook nog bedenken hoe we het online aspect toe gaan passen in het concept. Het feit dat de studenten geen tot weinig games spelen betekend niet dat ze geen games willen spelen.

----------
### BLOG
Tijdens de workshop: Blog Maken, hebben wij onze eerste introductie gehad met GitLab/Smartgit. Aan de hand van een aantal stappen moesten wij onze blog online zetten. Echter na het volgen van de stappen werkte bepaalde dingen niet, de bestanden die het programma die automatisch gedownload zouden worden kwamen maar niet. Uiteindelijk met wat hulp van de docent (en veel trial en error) bleek het opnieuw opstarten van het programma te helpen.

Conclusie: een blog maken met dit programma is vrij ingewikkeld als je niet weet hoe alles in elkaar zit.
