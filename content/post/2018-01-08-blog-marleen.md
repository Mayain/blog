---
title: Design Challenge - Eerste studiodag na de kerstvakantie
subtitle:
date: 2018-01-08
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge Op2"]
---

Samen met het team besproken wat er allemaal gedaan moet worden de komende periode. Ikzelf neem de visuele ondersteuning voor de pitch op mij, Simon wou graag werken aan de high fid prototype met de hulp van Danielle.
