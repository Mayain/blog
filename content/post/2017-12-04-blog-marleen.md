---
title: Design Challenge
subtitle:
date: 2017-12-04
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge OP2"]
---

Vandaag heb ik met mijn team gekeken naar het testplan. Simon en Ceasare zouden deze eigenlijk vorige week maken, maar hebben dit echter niet gedaan. De opzet die gemaakt was heb ik toen doorgelezen en aangepast waar dit nodig was. Ook hebben we het gehad over het testen van de prototypes. Een aantal van mijn team begrepen niet dat de 3 prototypes allemaal getest moeten worden maar dachten dat we ze samen moesten voegen tot een concept. Ik probeerde echter duidelijk te maken dat we ze alle 3 moesten testen en vanuit die data dan een concept uit te kiezen of onderdelen samen te voegen. Dit werd echter slecht tot niet opgepakt door mijn team, waardoor ik mij genegeerd en niet serieus genomen voelde.

Iedereen trok bij het testen/interviewen maar een beetje z’n eigen plan, dus ik heb toen maar besloten om mijn concept te laten bekijken door mijn vriend (die ook binnen de doelgroep valt), dit heeft hier vervolgens zijn feedback op gegeven.
S’avonds heb ik de pitch die Gabrielle had opgezet doorgelezen om te kijken of er nog iets aan misste. Ik zag toen dat ze ongeveer 2 A-4tjes vol had geschreven, wat veel te veel is om in een minuut te pitchen. Ik heb toen een kortte samenvatting van de pitch opgezet. Om de pitch te ondersteunen heb ik ook nog een simpele poster gemaakt met de 4 kernelementen van het concept.

![](Perry.JPG)
_Test prototype_
