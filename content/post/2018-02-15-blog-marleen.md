---
title:  Design Theory - Werkcollege
date: 2018-02-15
categorie: ["Design Theory OP3", "Werkcollege"]
tags: ["Design Theory OP3", "Werkcollege"]
---

Vandaag had ik het werkcollege na aanleiding van de theorie van afgelopen dinsdag. De groep werd opgedeeld in 4 kleine teams. Ik werd met Evenlien ingedeeld en wij moesten onderzoek doen naar de artifacten: paperclip en internet.

![](workshopartifact.jpg)
_Werkcollege artifacten_
