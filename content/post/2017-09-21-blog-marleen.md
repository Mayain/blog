---
title: Tools for Design - InDesign
subtitle:
date: 2017-09-21
categorie: ["Tools for Design", "Feedback"]
tags: ["InDesign", "SWOT", "Tools for Design", "Feedback"]
---

### Tools for Design: InDesign
Vandaag hadden wij uiteindelijk de eerste les van InDesign. In deze les kregen wij voornamelijk de basis van het programma uitgelegd. Dit was voor mij al allemaal vrij bekend, omdat ik zelf al in Photoshop en Illustrator gewerkt was dit voor mij al allemaal bekend. Dit komt doordat de interface van de Adobe programma's allemaal vrij overeenkomen.

----------
### Verwerking feedback
Na de Tools for Design hebben wij met de groep samen gewerkt aan de feedback die wij hebben ontvangen over de teamafspraken, teamregels, SWOT (Strength, Weakness, Oppertunities, Threats) en de teamdoelen. Wij moesten onze SWOT verder uitwerken en uitbreiden en de gezamelijke doelen stellen. Aan de hand van onze persoonlijke sterktes en zwaktes hebben we de swot op kunne stellen. Wij hebben hierbij gekeken naar de algemene dingen die bij ons overeenkwamen en wat wij in ons team missen. Tijdens het opstellen van de SWOT konden wij merken dat er in de groep onnodige discussies kunnen ontstaan die vrij lang doorgaan. Een van de oorzaken hiervan is dat een aantal in onze groep vrij eigenwijs kunnen zijn, waardoor je niet snel tot een keuze komt.
