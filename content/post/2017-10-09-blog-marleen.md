---
title: KISS
subtitle: (Keep It Simple Stupid)
date: 2017-10-09
categorie: ["KISS", "Iteratie 3", "Design Challenge"]
tags: ["KISS", "Iteratie 3", "Design Challenge"]
---

### KISS (Keep It Simple Stupid)
Vandaag kregen wij de briefing voor de derde iteratie. Dit wordt de kortste iteratie van de komende periode en daarom heet het ok KISS: Keep It Simple Stupid. Omdat de iteratie vrij kort is is het dus belangrijk om het simpel te houden. Wij hebben met de groep gekenen naar de feedback die wij vorige week onvangen hebben en gaan hiermee aan de slag.

Om het overzichtelijk te houden heb ik de belangrijkste punten van feedback in een overzicht met post-its uitgehangen, zoals de scrum methode. Zo kunnen we makkelijk zien wat er gedaan moet worden in deze korte periode. Wij hebben aan de hand van de dingen die verbetert moeten worden ook weer een planning opgesteld voor de komende periode. Morgen zullen we met z'n allen een rondje gaan lopen om te kijken hoe groot de Magic Circle van onze game wordt.

![](scrum planning.jpg)
_'Scrum' overzicht_
