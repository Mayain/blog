---
title: Design Theory - Debat
subtitle:
date: 2017-12-14
categorie: ["Design Theory", "Debat"]
tags: ["Design Theory", "Debat"]
---

Vandaag heb ik met de design Theory een video gekeken die ging over hoe men tegenwoordig mensen probeert te beïnvloeden door ze op maat gemaakte sites aan te bieden. Ookwel Persuation Profiling genoemd.

Hierover hebben wij vervolgens gedebatteerd over of er wel of geen regels verbonden moeten worden aan het gebruik van Persuation Profiling.
