---
title:  Design Theory media en context
date: 2018-02-20
categorie: ["Design Theory OP3", "Workshop"]
tags: ["Design Theory OP3", "Workshop"]
---

Vandaag had ik het tweede hoorcollege van het kwartaal. Ditmaal ging het over media en context. Hierbij kon je zien dat de context een belangrijke rol speelt in het ontwikkelen van een ontwerp en dat media ook grote invloed heeft op de mens en zijn leefstijl.

#### Workshop inrichtten ontwerpproces
Na het hoorcollege had ik de workshop inrichten ontwerpproces. Hier heb ik gekeken naar de indeling van het ontwerpproces voor het project. Ik heb uit het boek: Universele ontwerpmethodes gelezen over een aantal methodes die mij aanspreken en ik graag toe zou willen passen tijdens het project.
