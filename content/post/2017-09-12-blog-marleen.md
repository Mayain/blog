---
title: Gestalt Wetten
subtitle:
date: 2017-09-12
categorie: ["Hoorcollege", "Concept"]
tags: ["Verbeelden", "Gestalt Wetten", "Hoorcollege", "Concept"]
---
### Hoorcollege - Verbeelden
Vandaag hadden wij een hoorcollege over verbeelden. Hierbij ging het over hoe je met beeld kunt communiceren naar de mens en wat bepaalde beelden weer voor betekenis kunnen hebben.  Hierbij spelen de **gestalt wetten** een grote rol. Ik zelf was wel al bekend met een aantal van de gestald wetten, maar nog lang niet met allemaal en hoe ze toegepast kunnen worden. Dit was een zeer interessant en leerzaam hoorcollege. Ik hoop ook de gestalt wetten weer goed toe te kunnen passen in de opdrachten die gaan komen.

![](dummy_aantekeningen_verbeelden_01.jpg)
![](dummy_aantekeningen_verbeelden_02.jpg)
_Aantekeningen hoorcollege verbeelden_

----------
### Concept
Na het hoorcollege hebben we nog met de groep het basisconcept iets verder uitgewerkt. Je kan hierbij merken dat een aantal van onze groep al een stap te ver zijn met ontwikkelen, omdat ze al na beginnen te denken over bepaalde functionaliteit die nog niet van toepassing is op dit moment in het proces. Voor ons paperprototype gaat wij ook ieder twee schermen uitwerken die wij vervolgens voor ons game analyse kunnen gebruiken.

![](concept_get_the_building.jpg)
_Concept Get the Building_
