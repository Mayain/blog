---
title: Brainstorming
subtitle:
date: 2017-09-25
categorie: ["Design Challenge", "Ontwerpproces", "Mindmap", "Planning"]
tags: ["Ontwerpproces", "Brainstorm", "Mindmap", "Design Challenge", "Ontwerpproces"]
---

### Ontwerpproces
Met ons team hebben wij ons Ontwerpproces van de afgelopen 4 weken inzichtelijk gemaakt. Hierdoor is het nu overzichtelijk welke creatieve processen wij al gebruik van gemaakt hebben en welke wij eventueel in de volgende periode toe kunnen passen.

![](ontwerpproces.JPG)
_Ontwerpproces team_

----------
### Brainstorming
Vandaag hebben wij met de groep een mindmap brainstorm gedaan voor ons gameconcept. Hierbij hebben wij geprobeerd om zo breed mogelijk te denken. Echter hebben wij de resutaten van de mindmaps niet echt toe kunnen passen op een eventueel nieuw concept. Gezamelijk hebben wij ook nog bekeken wat er nog precies moet gebeuren en aan de hand hiervan een planning opgemaakt.

![](mindmap_01.jpg)
![](mindmap_02.jpg)
![](mindmap_03.jpg)
_Mindmaps team_
