---
title: Rotterdam Tour
subtitle:
date: 2017-08-31
categorie: ["Hoorcollege", "Tour"]
tags: ["Rotterdam Tour","Hoorcollege"]
---
### Hoorcollege
Via de lecture wat informatie ontvangen over de opleiding, hoe het eerste jaar in elkaar zit en wat je zoal moet halen om over te kunnen naar het volgende jaar.

### Rotterdam Tour
Na het hoorcollege moesten wij met ons groepje een tour door Rotterdam. Met deze tour kwamen wij langs verschillende iconische Rotterdamse gebouwen en kunstwerken. Dit leek mij eventueel een toepasselijk onderwerp voor het Rotterdamse thema voor onze game, dus ik heb een aantal foto's gemaakt ter inspiratie. Als afsluiter van de dag hebben we met z'n 5e nog een drankje gedronken op een terrasje.


![](rotterdam_01.JPG)
![](rotterdam_02.JPG)
![](rotterdam_03.JPG)
![](rotterdam_04.JPG)
![](rotterdam_05.JPG)
![](rotterdam_06.JPG)
![](rotterdam_07.JPG)
![](rotterdam_08.JPG)
![](rotterdam_09.JPG)
![](rotterdam_10.JPG)
_Rotterdam Tour_
