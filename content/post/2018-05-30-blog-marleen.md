---
title:  Design Challenge
date: 2018-05-30
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Vandaag zijn wij aan de slag gegaan met het uitwerken van het high-fid prototype. Ik heb het logo aan de hand van feedback van de respondenten nog verder uitgewerkt en beter leesbaar gemaakt. Daarnaast heb ik gekeken naar de eventuel kleurenpalet die wij zouden kunnen gebruiken voor het prototype. Ik heb Bram, Adriana en Verena ook geholpen met het uitwerken van de schermen.

Philip gaat de poster van het concept uitwerken. Aandgezien hij geen ervaring heeft met Illustartor heb ik hem ondersteund met het uitwerken van de poster.
