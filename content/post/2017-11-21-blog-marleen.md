---
title: Hoorcollege - De Mens - Persoonlijke Beinvloeding
subtitle:
date: 2017-11-21
categorie: ["Design Theory OP2"]
tags: ["Design Theory OP2"]
---

Vandaag was ons eerste hoorcollege van het nieuwe kwartaal. Dit kwartaal behandelen we: De Mens. Deze theorie zal zich meer richtten op het psycholigische aspect van het ontwerpen. Dit eerste hoorcollege ging over: Gedrag. Hoe wordt gedrag beinvloed en hoe kan je gedrag begrijpen? Hiervoor zijn een aantal verschillende teorieen die toegepast zouden kunnen worden.

![](Design Theory - persoonlijke beinvloeding 1.jpg)
![](Design Theory - persoonlijke beinvloeding 2.jpg)
_Aantekeningen hoorcollege_
