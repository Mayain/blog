---
title: Design Challenge
subtitle:
date: 2017-11-29
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge OP2"]
---
#### Studiecoaching
Vandaag hebben wij met studiecoaching weer gekeken naar onze nieuwe kwartaaldoelen. Ik heb hierbij gerefelcteerd op mijn vorige kwartaaldoelen: welke ik wel of niet gehaald heb en wat ik mee wil nemen naam het volgende kwartaal. Aan de hand daarvan heb ik weer nieuwe doelen gesteld voor mijzelf.

Onze studiecoach heeft ons ook wat advies gegeven over het werken in een team en hoe je elkaar sterktes het beste kan benutten. Hierbij is het belangrijk dat je eerlijk bent naar elkaar en ook goed communiceert onderling.

Thuis een low-fid paper prototype gemaakt aan de hand van mijn concept. Deze heb ik net uitgewerkt in simpele wireframes.

![](Concept 1.jpg)
![](Concept 2.jpg)
![](Concept 3.jpg)
_Low-fid prototype_
