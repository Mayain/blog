---
title: Kill Your Darlings
subtitle:
date: 2017-09-18
categorie: ["Design Challenge"]
tags: ["Kill Your Darlings", "Opnieuw", "Design Challenge"]
---

### KYD Design Challenge
Vandaag begonnen wij de dag met de conceptpresentaties van alle teams in de studio. Het was leuk om te zien hoeveel verschijdenheid er was in de concepten die iedereen presenteerde. Sverre en Evelien hebben voor onze groep de presentatie gehouden. Hierbij vergat Sverre aan het begin goed en duidelijk het concept uit te leggen. Dit is ietds waar aan gewerkt kan worden door wellicht de volgende keer de presentatie iets uitgebreider te maken. De feedback die wij op onze presentatie kregen was dat het kaart/bordspel aspect (wat onze doelgroep soms wel speelde) niet goed naar voren kwam, dat de uitleg van het spel nog niet duidelijk terug was te vinden in de presentatie van het prototype en op wat voor manier je in contact kan komen met jouw of andere teams.

Na de presentaties deden wij een gezamelijke lunch met de studio. Wij kregen toen ook de briefing van onze tweede iteratie: Kill Your Darlings [KYD]. Wij moeten nu onz huidige concept in de vuilnisbak gooien en alles weer opnieuw bekijken. Gaan we nog dezelfde doelgroep behouden? Behouden wij ons huidige thema? Of beginnen wij wellicht helemaal opnieuw?
