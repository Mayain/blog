---
title: Analyzing....
subtitle:
date: 2017-09-06
categorie: ["Analyseren", "Feedback", "Workshop", "Studie Coaching"]
tags: ["Interview Analyseren", "Workshop", "Zelfsturing", "Studie Coaching"]
---
### Studio dag: interviews analyseren
Vandaag hebben we onze eigen interviews terugluisteren en uitgeschreven. Met het terugluisteren van mijn interview merkte ik dat ik eigenlijk te weinig door vroeg op de antwoorden die er gegeven werden. Tijdens het terugluisteren van het interview kwamen er eigenlijk al weer meer vragen naar boven die ik eigenlijk weer had kunnen vragen na aanleiding van het antwoord wat gegeven werd. Dit is dan ook een leermoment die ik mee wil nemen naar de toekomst, voor als ik weer eens een interview af moet nemen.

### Feedback
Als feedback kregen wij vandaag het advies om alle interviews goed te analyseren. Hierdoor zou je wellicht interessante punten of ideeën tegen kunnen komen die eventueel weer bruikbaar zouden kunnen zijn voor het concept van de game.

### Workshop  
Na de lunch kregen we een korte workshop over het maken van een blog. Hierbij werd verteld dat het verstandig is om, als je vaktermen hoor of tegenkomt, op te zoeken wat deze betekenen en deze ook eigen te maken. Tijdens de workshop hebben informatie gehad waarom er HTML gebruikt wordt voor sites en wat een makkelijke manier is om een site

### Studie coaching
We hebben vandaag ook voor het eerst kennis gemaakt met de studie coaching. Hierbij moesten we kijken naar onze SWOT (strength, weakness, oppertunities, triumphs) binnen het team. Aan de hand van de punten die wij vinden zullen wij ook weer een poster moeten maken voor ons team.

![](inventarisatie_team_01.jpg)
![](inventarisatie_team_02.jpg)
_Inventarisatie team_

Tijdens deze les is er ook nog gesproken over zelfsturing. Dit houdt in dat als je iets wilt of enige vraag hebt zelf met het initiatief komt om het te regelen of er wellicht een oplossing voor te vinden. Hierbij speel communicatie met elkaar een belangrijke rol en is het belangrijk dat je het overlegt met met de mensen in de studio. Echter voordat er iets mag/kan gebeuren dien je wel het te overleggen met de studiebegeleider of studiecoach.
