---
title: Eerste assesment
subtitle:
date: 2017-11-10
categorie: ["Assesment", "Feedforward"]
tags: ["Assement", "Feedforward"]
---
### Eerste assesment
Vandaag had ik mijn eerste assesment van het jaar. Dit assesment gesprek verliep voor mijn gevoel goed. Ik kon alle vragen die er waren gewoon goed beantwoorden en aanvullen waar nodig.

----------
### Feedforward
Het leerdossier wat ik heb opgeleverd was goed en goed verzorgd. Ik moet echter de volgende keer niet vergeten om de checklist op het voorblad af te vinken en beter letten op spelling.

Mijn feedforward voor de volgende periode is:
- Ik moet refecteren op de competenties aan de hand van de kertaken en gedragsindicatoren die hierbij horen.
-  D.m.v. onderzoeksbewijzen anderen van zoncepten of bepaalde keuzes overtuigen.  
- Het aankomend kwartaal mijzelf anders opstellen binnen de groep, en dus niet als teamleiden funtioneren (bijv. visueel uitwerken met beredenering voor bepaalde ontwerpkeuzes).
- Meer mensen betrekken in het ontwerpproces (gebruiker, docenten, andere teams) om zo tot betere keuzes te komen.
- Kennis zoeken buiten mijn eigen kring (vakliterartuur, mediatheek, lezingen, TED-talks, podcasts,vakdocenten ect.).
