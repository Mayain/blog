---
title: Prototype 3
subtitle:
date: 2017-10-23
categorie: ["Design Theory", "Prototype"]
tags: ["Design Theory", "Prototype"]
---
### Ontwerpproceskaart
Vandaag hebben wij weer gekeken naar het ontwerpproces van de afgelopen periode en deze weer duidelijk in kaart gebracht.

----------
### Prototype nr. 3
Daarnaast hebben wij met de groep de puntjes op de i gezet voor voor de expo van aankomende woensdag. Ik heb vandaag een nieuwe versie van onze Mystery Box in elkaar gezet en het concept gevisualiseerd. De visualisatie van het concept zal ons helpen ondersteunen bij het presenteren van het concept tijdens de expo.
