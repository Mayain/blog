---
title:  Design Challenge
date: 2018-05-14
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Vandaag moest ik met het team een concept bedenken voor de nieuwe gebruikers. Hiervoor hebben wij samen gebrainstormd. Hiervoor hebben wij een twee mindmaps opgezet. Eentje over slaap en eentje over schermen. Bij deze woorden hebben wij woorden bedacht die hierbij aansluiten en in ons opkwamen. Vervolgens hebben we woorden aan elkaar gelinkt. Vanuit deze gelinkte woorden hebben wij 3 ideeën gepakt: het brainwaves kussen, beeldschermen langzaam dimmen en het personal sleeping profile. Deze concepten zullen wij nog verder uit gaan werken.

![](IMG_0746.jpg)
![](IMG_0747.jpg)
![](IMG_0748.jpg)
_Mindmaps voor concept ideeën_
