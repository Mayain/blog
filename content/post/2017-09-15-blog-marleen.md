---
title: Eerste Deadline
subtitle:
date: 2017-09-15
categorie: ["Deadline", "Huiswerk"]
tags: ["Iteratie 1", "Dit ben ik"]
---
### Deadline Iteratie 1
Vandaag hadden wij onze aller eerste deadline voor de eerste iteratie. Hiervoor heb ik thuis nog goed na zitten denken over het 'Dit ben ik-profiel' wat we in moesten leveren. Ik het de rollen quiz nog een geanalyseerd en daar de punten uitgehaald die wel of niet van toepassing op mij zijn. Aan de hand van die punten heb ik een SWOT analyse gemaakt. Het moeilijkste van het gehele 'Dit ben ik-profiel' blijft voor mij nog wel de kwartaaldoelen stellen. Dit omdat ik het moeilijk vindt om hier echt goed op door te denken en ook te kijken wat ik nou echt wil bereiken. Ik als persoon zijnde ben altijd meer een doener dan een denker geweest, hierdoor is het voor mij best moeilijk om op mijn werk te refecteren.

Een groot deel van het geheel had ik al eerder gemaakt, dus toen was het enkel nog een kwestie van het samen te bundelen in een PDF en het te uploaden op N@tschool.
