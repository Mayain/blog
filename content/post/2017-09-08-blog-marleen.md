---
title: Een concept bedenken
subtitle:
date: 2017-09-08
categorie: ["Analyseren","Concept Brainstorm"]
tags: ["Rotterdam", "Concept", "Puzzle", "Analyseren"]
---
### Huiswerk
Vandaag heb ik de interviews die wij afgelopen week afgenomen hebben geanalyseerd. Een aantal van de belangrijkse punten in de interviews waren:

 - Opleiding: conceptueel/creatief en technisch
 - Houden van gemak (voorbeeld: niet ver reizen naar school)
 - Zijn over het algemeen nog niet zo bekend iconische Rotterdamse gebouwen
 - Rotterdam: oud- en nieuwbouw
 - Rotterdam: veel type gebouwen en bebouwing, stad die zichzelf blijf ontwikkelen
 - Rotterdam: metropool
 - Gamen niet tot weinig
 - Bij games draait het om gezelligheid of eventuele voldoening
 - Facebook & Instagram meest gebruikte social media

Vervolgens ben ik na gaan denken over een concept voor onze game. Ik vindt het hierbij belangrijk dat de studenten fysieke interactie met elkaar hebben (voorbeeld: kaarten). Op die manier leer je elkaar sneller kennen dan als dat eventueel met een applicatie zou gaan. Ik wou het ook simpel houden, omdat de doelgroep niet veel games speelt. na een beetje brainstormen kwam ik bij het idee om eventuele invloeden van een escape room er bij te betrekken. Dit omdat je in een escape room goed samen moet werken, hiermee leer je elkaar dan al snel kennen.

Mijn concept draait om een doos. Deze doos zit gesloten met sleutels. Het doel is om de doos zo snel mogelijk open te krijgen doodmiddel van het oplossen van vragen/puzzels/opdrachten via een app.

![](concept_the_box.jpg)
_Gameconcept_
