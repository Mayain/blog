---
title:  Design Theory
date: 2018-02-22
categorie: ["Design Theory OP3", "Werkcollege"]
tags: ["Design Theory OP3", "Werkcollege"]
---

Werkcollege over de theorie van afgelopen dinsdag. Ik moest samen met Rick een artefact uitkiezen en gaat kijken hoe de context het artefact in de loop van de jaren heeft laten doen veranderen.

![](werkcollege2.jpg)
_Artefact en context_
