---
title: Concept Presentatie
subtitle:
date: 2017-10-04
categorie: ["Presentatie", "Studie Coaching"]
tags: ["Concept", "Presentatie", "Studie Coaching", "STARR(T)"]
---

### Concept Presentatie
Vandaag hadden wij onze presentatie van het concept. Ditmaal was  de presentatie voor twee doncenten en een alumini. Ik mocht het gedeelte over de kern van het concept presenteren. Dit verliep vrij goed, achter had ik toch nog wel last van wat zenuwen waardoor het niet precies verliep zoals ik in mijn hoofd had zitten. Ik vergat bijne te vertellen dat de studenten een doos per groep ontvangen. Gelukkig realiseerde ik mij dit optijd en kon ik het nog goed in mijn verhaal er in passen.

-----------
### Feedback
Aan de hand van onze presentatie ontvingen wij ook nog enige feedback. Deze feedback willen wij in de volgende iteratie verwerken om zo de puntjes op de i te kunnen zetten.

![](feedback (2).JPG)
Het feit dat ons concept als sterk word gezien bevalt ons. We zullen proberen in de volgende iteratie
het competitie en samen spel nog meer uitwerken. Onze doelgroep word inderdaad op de juiste
manier benaderd en ons concept is zo goed als mogelijk gericht op de doelgroep. Het vraag gedeelte
van ons concept staat nu nog in de kinderschoenen, we hebben pas twee vragen geformuleerd en de
moeilijkheidsgraad verdeling moet nog worden bepaald, een eventuele straf per fout antwoord kan
de spelers motiveren om de juiste antwoorden te geven. Bedankt voor deze positieve feedback.

![](feedback (1).JPG)
Het is op dit moment geheim wat de eindopdracht is en wat er in de doos zit. Dit moeten we nog
verder uitwerken en eventueel testen met een A B test om te kijken wat beter bevalt. We proberen
inderdaad de doelgroep vooraan te zetten in ons concept en we waarderen uw feedback die
aangeeft dat wij dit goed doen. Verdere uitwerking van het concept is waar we ons nu op gaan
richten waarbij we tot in de details alles uitwerken. Hoe deze details worden ingeleverd moet nog
blijken. Fijn dat u genoot van onze presentatie.


![](feedback (3).JPG)
Een groot gedeelte van onze game is inderdaad het beantwoorden van quiz vragen. Voor deze
iteratie hebben we maar twee vragen als voorbeeld bedacht, hoe we de vragen qua moeilijkheid en
type gaan rangschikken is iets waar we de volgende iteratie aan gaan werken. Voor de eind maquette
hebben we op dit moment alleen één klein huisje gemaakt, verdere details van hoe en wat van de
maquette moeten nog verfijnd worden. Andere details zoals het “speelveld” (magic cirle) zijn tot nu
toe nog niet bepaald, dit behoort tot details die we komende iteratie gaan uitwerken.

----------
#### STARR(T)
Na de presentaties hadden wij nog les van de studie coaches. Vandaag ging het over het schrijven van een *STARR(T)*:
- *Situatie*:     In welke situatie bevond jij je?
- *Taak*:         Wat was jouw taak en/of opdracht? Wat werd van jou verwacht?
- *Actie*:        Welke acties heb je ondernomen.
                  Hoe heb je de taak opdracht aangepakt? Waarom?
                  Hoe heb je het opgelost als je tegen een probleem aanliep?
- *Resultaat*:    Wat was het resultaat van jouw acties?
- *Reflectie*:    Wat is het belangrijkste dat je geleerd hebt?
                  Welke kwaliteiten kwamen tot uiting? (minimaal 2 punten) ?
                  Wat zou je de volgende keer anders doen en waarom?
                  Andere kwaliteiten/competenties die blijken uit deze STARR(T)?
- *Transfer*:     Voor het leerdossier wordt de ‘T’ van transfer erbij opgenomen.
                  Welke leerdoelen neem jij mee naar kwartaal 2?Situatie, Actie, Resultaat, Reflectie, Transfer. Aan de hand van deze

Aan de hand van deze punten moet ik mijn competenties aantonen in mijn leerdossier die ik het eind van dit kwartaal op moet leveren.
