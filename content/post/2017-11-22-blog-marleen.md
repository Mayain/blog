---
title: Studiodag 2 - Het te laat komen continues...
subtitle:
date: 2017-11-22
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge OP2"]
---

Vandaag begon de ochtend met Danielle en mijzelf in de studio. De jongens hadden zich vandaag weer verslapen. Na aanleiding van hun (nu al) regelmatig te laat komen ga ik vanaf vandaag ook een document bijhouden voor het te laat komen en de beredenering die iedereen er voor heeft. Mocht dit dan regelmatig voorkomen, dat kan ik doormiddel van dat document aantonen hoe vaak ze al te laat zijn gekomen.
