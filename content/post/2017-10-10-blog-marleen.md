---
title: Ziek
subtitle:
date: 2017-10-10
categorie: [""]
tags: ["STARR(T)"]
---

### Ziek
Vandaag was de planning om met de groep een rondje te gaan lopen voor de Magic Circle van onze game, echter kon ik door ziekte helaas niet mee lopen. Ik heb toen thuis wel een eerste STARR(T) uitgeschreven voor de les van morgen. Hierbij heb ik voor de competentie 'Onderzoeken' gekeken naar de interviews die ik heb afgenomen de afgelopen periodes en hier op gereflecteerd wat ik mee zou kunnen nemen naar de volgende periode.
