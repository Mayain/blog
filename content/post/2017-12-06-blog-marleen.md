---
title: Pitch
subtitle:
date: 2017-12-06
categorie: ["Design Challenge OP2", "Pitch"]
tags: ["Design Challenge OP2", "Pitch"]
---
##Design Challenge - pitch
Vandaag was de pitch voor ons concept. Wonder boven wonder was iedereen vandaag gewoon optijd op school. De pitch die Gabrielle en Simon vervolgens hielden ging goed, we kregen voornamelijk positieve feedback op ons concept, feedback waarmee wij weer goed aan de slag kunnen gaan.

Na de pitch heb ik bij mijn team aangegeven dat de communicatie en samenwerking wel verbeterd moet worden, zodat het voor iedereen ook duidelijk is wat voor data er nu allemaal verzameld is. Momenteel mis ik namelijk alle informatie die anderen uit hun onderzoek gehaald hebben.

![](pitch.JPG)
_Pitch concept_
