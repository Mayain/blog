---
title:  Design Challenge
date: 2018-04-11
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Voor de nieuwe periode moest ik samen met het team een nieuw plan van aanpak maken, ditmaal gericht op de jongeren van Rotterdam Zuid. Hierbij heb ik invulling gegeven in de planning van het project. Voor de komende periode heb ik een planning geschetst en uitgewerkt.

![](Plan van aanpak V2.jpg)
_Plan van aanpak_
