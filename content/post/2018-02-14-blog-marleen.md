---
title:  Design Challenge
date: 2018-02-14
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag kregen wij feedback op het plan van aanpak wat wij afgelopen maandag opgesteld hadden. Wij moesten onze plan van aanpak visueler uitweken aan de hand van een poster, de planning uitgebreider opstellen en en de onderzoeksplan persoonlijker formuleren. Verena is aan de slag gegaan met het visualiseren van het plan van aanpak en ik ben met de rest van het team gaan kijken naar de persoonlijkere invuling van het onderzoeksplan.
