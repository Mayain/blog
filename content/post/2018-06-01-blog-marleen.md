---
title:  Design Challenge
date: 2018-06-01
categorie: ["Design Challenge OP4"]
tags: ["Design Challenge OP4"]
---

Vandaag was het zo ver, de eindpresentatie van ons concept. De opdrachtgevers vanuit EMI waren zee enthousiast over ons concept, echter mistte de link met de wijk en werden wij hierdoor niet uitgeselecteerd als een van de winnende 8 teams.
EMI wil wel graag op hun website kunnen laten zien wat wij ontwikkelnd hebben in de afgelopen periode en zal hiervoor nog contact met ons opnemen.

Een zeer geslaagde expo!

![](IMG_0877.jpg)
![](IMG_0887.jpg)
![](IMG_0872.jpg)
_Cheers!_
