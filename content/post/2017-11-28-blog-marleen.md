---
title: Hoorcollege - Emotionele Beinvloeding
subtitle:
date: 2017-11-28
categorie: ["Design Theory OP2", "Emoties"]
tags: ["Design Theory OP2", "Emoties"]
---

Vandaag ging het hoorcollege over emitionele beinvloeding. We hebben gekeken naar hoe emoties jouw beslissingen kunnen beinvloeden en hoe je dit weer in positieve en negatieve zin kan gebruiken.

![](Design Theory - emotionele beinvoeding 1.jpg)
![](Design Theory - emotionele beinvoeding 2.jpg)
_Aantekeningen Hoorcollege_
