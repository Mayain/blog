---
title: Workshop - Letterontwerpen
subtitle:
date: 2017-12-12
categorie: ["Workshop", "Letterontwerpen"]
tags: ["Workshop", "Letterontwerpen"]
---

Vandaag had ik de workshop letterontwerpen. We kregen de basis over fonts en hoe je ze opbouwt uitgelegd. Vervolgens gingen we zelf aan de slag en hebben wij geoefend door verschillende fonts op verschillende manieren uit te schrijven.

![](letterontwerpen_2.jpg)
_Workshop Letterontwerpen_
