---
title: Eerste studiodag OP2
subtitle:
date: 2017-11-20
categorie: ["Design Challenge OP2"]
tags: ["Design Challenge OP2"]
---

Vandaag was onze eeste studiodag van het team. Deze dag begon voor mij zeer goed, omdat ik de enige van mijn team was die s'ochtends aanwezig was in de studio. De reden hiervoor was dat Gabrielle de komende week op familiebezoek is in het buitenland, Danielle was problemen heeft met haar moeder en Ceasare en Simon zich verslapen hadden. Na aanleiding van het feit dat ik aleen aan de tafel zat kwamen Mio en Jantien ook gelijk bij mij zitten om te bespreken waarom ik alleen was. Zij gaven hiervoor de feedback dat ik gewoon een goed geprek met de teamladen aan moet gaan als ze te laat komen en ze eventueel moet helpen of ondersteuen als ze moeite hebben om optijd te komen.

Toen de jongens eenmaal aangekomen waren heb ik ze er ook mee geconfronteerd. Ceasare die doet er vrij makkelijk mee en die ziet de ernst er niet zo van in. Simon zei dat hij het wel erg vond en wil proberen er iets aan te doen. voor beide jongens is et voor mij nu nog maar afwachtten hoe het gaat lopen, maar ze maken wel gelijk een slechte start zo aan het begin van het project.

Ik heb de rest van de studiodag verdergewerkt aan mijn merkanalyse. Hiervoor heb ik eerst doorgenomen wat een merkanalyse precies in houd en vervolgens ben ik begonnen met het doen van onderzoek. Ik ben op de website van PAARD gaan zoeken naar informatie. Hier kwam ik hun beleidsplan (https://www.paard.nl/wp-content/uploads/2017/07/PVT_BELEIDSPLAN-2017-2020.pdf) tegen van de afgelopen jaren en voor de toekomst. Dit document heb ik doorgelezen en aan de hand daarvan de merkanalyse opgesteld.


![](onderzoek markanalyse.jpg)
_Onderzoek merkanalyse_
