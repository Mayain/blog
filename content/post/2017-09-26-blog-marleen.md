---
title: Concept nr. 2
subtitle:
date: 2017-09-26
categorie: ["Hoorcollege", "Onderzoeken", "Concept"]
tags: ["Onderzoeken", "Concept", "Brainstorm", "Steentje Bijdragen", "Hoorcollege", "Onderzoeken"]
---
### Hoorcollege: Onderzoeken
Vandaag ging ons hoorcollege over onderzoeken en het ontwerpproces wat wij als CMders doorlopen.

![](onderzoeken 1.jpg)
![](onderzoeken 2.jpg)
_Aantekeningen hoorcollege: Onderzoeken_

----------
### Concept nr. 2
Voor de nieuwe iteratie heb ik de basis voor een nieuw concept opgezet. Dit concept noem ik: Steentje bijdragen. Het idee is dat er door Rotterdam verschillende onderdelen van een manquette verspreid liggen. De leerlingen worder in groepen opgedeeld en moeten samen opzoek naar de maquetteonderdelen, zodat zij deze in elkaar kunnen bouwen. Doormiddel van een App creeer je een team en krijg je de eerste hint voor de eerste locatie. Op het onderdeel wat ze vinden staan QR codes. Deze worden ingescand op je mobiel, waardoor er vervolgens een aantal vragen gesteld wordt. De antwoorden van de vragen verschijnen op de telefoons van de andere teamleden. Zijn de vragen juist beantwoord kijgt de groep een nieuwe hint voor de nieuwe locatie. Zodra zij dan alle maquetteonderdelen gevonden hebben moeten zij deze in elkaar zetten. Het team wat dit als eerste lukt, win.

![](steentje bijdragen.jpg)
_Concept: Steentje bijdragen_

----------
### Concept Brainstorm
Gezamelijk hebben wij weer gekeken naar de nieuwe concepten die er bedacht zijn. Evelien kwam met het idee om iets in het de stijl van wie is de mol te ontwikkelen en Rick had het idee voor een soort van levend Stratego. Het concept van Evelien miste de 'Meaningfull Play' en bij Rick miste de het 'elkaar leren kennen' in het concept. Uiteindelijk hebben wij er samen voor gekozen om de concepten die wij in de eerste iteratie hebben bedacht weer tevoorschijn te halen. Hierbij was het concept 'The Box' (Met een team de doos zo snel mogelijk open zien te krijgen) er een waar wij op verder zijn gaan denken, omdat bij dit concept competite en teamplay goed naar voren komen. Iris kwam daarbij met het idee om het spel te versprijden over meedere dagen en elke dag dan een ander Rotterdams thema te tackelen. zo kunnen de teams per dag 1 sleutel of code verdienen waarmee ze dichterbij het openen van de doos komen.

![](the box 2.jpg)
_Concept: The Box 2.0_
