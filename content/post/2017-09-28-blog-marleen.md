---
title: Onderzoek uitvoeren
subtitle:
date: 2017-09-28
categorie: ["Design Theory", "Onderzoek", "Paper Prototype", "Tools for Design"]
tags: ["Onderzoeken", "Paper Prototype", "Data", "Design Theory", "Tools for Design"]
---
### Tools for Design - Indesign
Vandaag was de tweede les voor InDesign. Ditmaal ging het over hoe je afbeeldingen kan gebruiken. Dit komt veel overeen zoals ik het ken in Photoshop en Illustrator. Het handige in InDesign is dat je er een tool voor heb en in Photoshop of illustrator maak je gebruik van een mask. Ook de effecten die je met afbeeldingen kan gebruiken komen weer overeen met de effecten die bruikbaar zijn in de andere Adobe programma's. Na aanleiding van de les moesten ik twee opdrachten maken. Bij bijde opdrachten moest ik een afbeelding na maken en hierbij de dingen die in de les uitgelegd waren toe passen (gebruik van afbeeldingen en effecten). Dit ging mij goed af, omdat ik al ervaring heb met de andere Adobe programma's en InDesign veel lijkt op de andere programma's.

----------
### Design Theory: Data
Vandaag met de design theory ging het over het ctagoriseren van data. wij kregen een bak met legoblokjes die data representeerd. Voor deze data moesten wij catagorien bedenken waarin wij deze data gingen verdelen. Vervolgens moesten wij kiezen uit 2 van de catagorien en de legoblokjes hierover verdelen. Wij kozen hierbij voor licht naar donker en weinig naar veel nopjes. Tijdens het verdelen van de legoblokjes kwamen wij er al achter dat sommig blokjes (data) in bepaalde catagorien niet thuishoren. Hierbij leerden wij dat je niet persee alle data hoeft te gebruiken. De data die buiten de kaders van de catagorie valt kan dan dus gewoon geschrapt worden. Enkel de data die dus van belang is hoef je te gebruiken.

Hierna moesten wij dezelfde opdracht nogmaals doen, alleen dan met foto's. Er waren hierbij minder catagorien die wij konden bedenken, hierdoor werd het verdelen van de foto's makkelijker.

![](data_analyseren_01.JPG)
![](data_analyseren_02.JPG)
_Analysatie data_
