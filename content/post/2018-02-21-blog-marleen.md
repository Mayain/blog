---
title:  Design Challenge
date: 2018-02-21
categorie: ["Design Challenge OP3"]
tags: ["Design Challenge OP3"]
---

Vandaag heb ik teruggekeken naar de feedback die ik heb ontvangen op mijn leerdossier van het afgelopen kwartaal. Ik had hierbij meer competenties deels afgesloten dan ik had begrepen. De competentie samenwerken heb ik nu afgerond, professionaliseren 1 en onderzoeken 1 en 2 heb ik voldoende behaald. Ik wil mij de komende periode graag focussen op de dataanalyse en het visueel uitwerken.

![](leerdossierfeedback1.jpg)
![](leerdossierfeedback2.jpg)
_Refectie feedback leerdossier_

#### Workshop visuele etnografie
De workshop etnografie ging over hoe je beelden kan lezen en welke informatie je dan allemaal uit die beelden kunt halen. Dankzij deze workshop kregen ik en mijn teamleden het idee om in de vakantie foto's te maken van ons voedsel, zodat wij dit vervolgens met elkaar kunnen vergelijken.
